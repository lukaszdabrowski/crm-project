package pl.sda;

public enum CustomerStatus {
    ImportedLead,
    NotContacted,
    Contacted,
    Customer,
    ClosedLost;
}
